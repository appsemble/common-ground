# Appsemble Common Ground

This repository contains the script used to generate the
[Common Ground](https://commonground-checklist.appsemble.appsemble.app)
[Appsemble](https://gitlab.com/appsemble/appsemble) app.

The script found at `src/index.ts` uses
[this yaml file](https://gitlab.com/commonground/docs/checklist/-/blob/master/checklist.yaml) from
the [Common Ground checklist repository](https://gitlab.com/commonground/docs/checklist) as the
basis to generate an Appsemble app with the option to fill in and save the checklist. This checklist
can then later be modified or linked to other users.

The files in `apps/common-ground` contain the app output and is used to update the Appsemble app
that is currently live.
