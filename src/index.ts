import { promises as fs } from 'fs';
import { join } from 'path';

import { AppDefinition, FlowPageDefinition } from '@appsemble/types';
import axios from 'axios';
import * as dedent from 'dedent';
import { dump, load } from 'js-yaml';
import { format, resolveConfig } from 'prettier';

interface Item {
  name: string;
  practice?: string;
  smart: string[];
  norms: string[];
  principles: string[];
}

interface CheckListResult {
  items: Item[];
  principles: Omit<Item, 'principles'>[];
}

function createSubPage(item: Item): FlowPageDefinition['subPages'][number] {
  const [code] = item.name.split(' ');
  return {
    name: code,
    blocks: [
      {
        type: 'markdown',
        version: '0.18.8',
        parameters: {
          content: dedent(`
        ## ${item.name}
        ${
          item.practice
            ? `### Praktijk
        ${item.practice}`
            : ''
        }
        ### SMART
        ${item.smart.map((smart, index) => `${index + 1}. ${smart}`).join('\n')}`),
        },
      },
      {
        type: 'form',
        version: '0.18.8',
        actions: {
          onPrevious: { type: 'flow.back' },
          onSubmit: { type: 'flow.next' },
        },
        parameters: {
          previous: true,
          fields: [
            {
              type: 'radio',
              name: code,
              options: [
                {
                  label: 'Niet compatibel',
                  value: 0,
                },
                ...item.norms.map((norm, index) => ({
                  label: norm,
                  value: index + 1,
                })),
              ],
            },
            {
              name: `${code}-comment`,
              type: 'string',
              label: 'Opmerking',
              multiline: true,
            },
          ],
        },
      },
    ],
  };
}

const template = (checklist: CheckListResult): AppDefinition => ({
  name: 'Commonground Checklist App',
  defaultLanguage: 'nl',
  defaultPage: 'Item Checklist',
  security: {
    default: {
      role: 'User',
    },
    roles: {
      User: { description: 'A user who can fill in the checklist.' },
    },
  },
  resources: {
    result: {
      roles: ['User'],
      query: { roles: ['$author'] },
      get: { roles: ['$public'] },
      schema: {
        type: 'object',
        required: ['projectName'],
        properties: {
          projectName: {
            type: 'string',
          },
          ...Object.fromEntries(
            checklist.items.flatMap((item) => [
              [
                String(item.name.split(' ')[0]),
                {
                  type: 'integer',
                  minimum: 0,
                  maximum: item.norms.length,
                  description: item.practice ?? '',
                },
              ],
              [
                `${item.name.split(' ')[0]}-comment`,
                {
                  type: 'string',
                  description: `De opmerking voor ${item.name.split(' ')[0]}`,
                },
              ],
            ]),
          ),
        },
      },
    },
  },
  pages: [
    {
      name: 'Checklists',
      roles: ['User'],
      blocks: [
        {
          type: 'data-loader',
          version: '0.18.8',
          actions: { onLoad: { type: 'resource.query', resource: 'result' } },
          events: { emit: { data: 'checklists' } },
        },
        {
          type: 'table',
          version: '0.18.8',
          events: {
            listen: { data: 'checklists' },
          },
          actions: {
            onClick: {
              type: 'link',
              to: 'Bekijk Checklist',
            },
          },
          parameters: {
            fields: [
              { value: { prop: 'projectName' }, label: 'Projectnaam' },
              {
                value: {
                  'string.format': {
                    template: '{date, date, medium}',
                    values: {
                      date: [{ prop: '$created' }, { 'date.parse': '' }],
                    },
                  },
                },
                label: 'Aanmaakdatum',
              },
            ],
          },
        },
      ],
    },
    {
      name: 'Item Checklist',
      roles: ['User'],
      type: 'flow',
      actions: {
        // @ts-expect-error This is a bug in the Appsemble types
        onFlowFinish: {
          type: 'resource.create',
          resource: 'result',
          onSuccess: { type: 'link', to: 'Bekijk Checklist' },
        },
      },
      subPages: [
        {
          name: 'Projectnaam',
          blocks: [
            {
              type: 'form',
              version: '0.18.8',
              actions: {
                onSubmit: { type: 'flow.next' },
              },
              parameters: {
                fields: [
                  {
                    name: 'projectName',
                    type: 'string',
                    requirements: [{ required: true }],
                    label: 'Projectnaam',
                  },
                ],
              },
            },
          ],
        },
      ],
    },
    {
      name: 'Checklist Bewerken',
      parameters: ['id'],
      roles: ['User'],
      type: 'flow',
      actions: {
        // @ts-expect-error This is a bug in the Appsemble types
        onFlowFinish: {
          type: 'resource.update',
          resource: 'result',
          onSuccess: { type: 'link', to: 'Bekijk Checklist' },
        },
      },
      subPages: [
        {
          name: 'Projectnaam',
          blocks: [
            {
              type: 'data-loader',
              version: '0.18.8',
              events: { emit: { data: 'result' } },
              actions: { onLoad: { type: 'resource.get', resource: 'result' } },
            },
            {
              type: 'form',
              version: '0.18.8',
              actions: {
                onSubmit: { type: 'flow.next' },
              },
              events: { listen: { data: 'result' } },
              parameters: {
                fields: [
                  {
                    name: 'projectName',
                    type: 'string',
                    requirements: [{ required: true }],
                    label: 'Projectnaam',
                  },
                ],
              },
            },
          ],
        },
      ],
    },
    {
      name: 'Bekijk Checklist',
      parameters: ['id'],
      blocks: [
        {
          type: 'action-button',
          version: '0.18.8',
          actions: { onClick: { type: 'link', to: 'Checklist Bewerken' } },
          parameters: { icon: 'pen' },
        },
        {
          type: 'data-loader',
          version: '0.18.8',
          actions: { onLoad: { type: 'resource.get', resource: 'result' } },
          events: { emit: { data: 'checklist' } },
        },
        ...checklist.items.flatMap((item) => [
          {
            type: 'markdown',
            version: '0.18.8',
            parameters: {
              content: dedent(`
        ## ${item.name}
        ${
          item.practice
            ? `### Praktijk
        ${item.practice}`
            : ''
        }
        ### SMART
        ${item.smart.map((smart, index) => `${index + 1}. ${smart}`).join('\n')}

        ### Normen
        ${item.norms.map((norm, index) => `${index + 1}. ${norm}`).join('\n')}`),
            },
          },
          {
            type: 'detail-viewer',
            version: '0.18.8',
            events: { listen: { data: 'checklist' } },
            parameters: {
              fields: [
                {
                  label: 'Score',
                  value: {
                    if: {
                      condition: { prop: item.name.split(' ')[0] },
                      then: { prop: item.name.split(' ')[0] },
                      else: {
                        if: {
                          condition: {
                            equals: [{ prop: item.name.split(' ')[0] }, { static: 0 }],
                          },
                          then: 'Niet compatibel',
                          else: 'Niet ingevuld',
                        },
                      },
                    },
                  },
                },
                {
                  label: 'Opmerking',
                  value: {
                    if: {
                      condition: { prop: `${item.name.split(' ')[0]}-comment` },
                      then: { prop: `${item.name.split(' ')[0]}-comment` },
                      else: 'Geen opmerking',
                    },
                  },
                },
              ],
            },
          },
        ]),
      ],
    },
  ],
});

async function main(): Promise<void> {
  const url = 'https://gitlab.com/commonground/docs/checklist/-/raw/master/checklist.yaml';
  const { data } = await axios.get<string>(url);
  const parsed = load(data) as CheckListResult;
  const result = template(parsed);

  for (const item of parsed.items) {
    (result.pages[1] as FlowPageDefinition).subPages.push(createSubPage(item));
    (result.pages[2] as FlowPageDefinition).subPages.push(createSubPage(item));
  }

  const filepath = join(__dirname, '..', 'apps', 'common-ground', 'app.yaml');
  const prettierConfig = await resolveConfig(filepath, { editorconfig: true });
  await fs.writeFile(filepath, format(dump(result), { parser: 'yaml', ...prettierConfig }));
}

main().then(() => console.log('Finished generating app.'));
